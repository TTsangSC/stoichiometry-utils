"""
Utilities for handling stoichiometries.
"""
from ._types import (
    IncompatibleObjectError,
    IncompatibleObjectTypeError,
    IncompatibleObjectValueError,
)
from .core import (
    Stoichiometry,
    vectorize_atoms,
    list_all_elements,
)
from .decomposition import (
    stoichiometrize_species,
)

__version__ = '1.1.0'

__all__ = (
    'IncompatibleObjectError',
    'IncompatibleObjectTypeError',
    'IncompatibleObjectValueError',
    'Stoichiometry',
    'list_all_elements',
    'stoichiometrize_species',
    'vectorize_atoms',
)
