"""
Typing stuff.
"""
import numbers
import typing
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal
    typing.Literal = Literal
try:
    from numpy.typing import NDArray
except ImportError:
    try:
        from typing import Protocol
    except ImportError:
        from typing_extensions import Protocol

    from numpy import ndarray

    class NDArray(Protocol[typing.TypeVar('Scalar')]):
        for attr_name, attr_value in vars(ndarray).items():
            if attr_name != '__class_getitem__':
                locals()[attr_name] = attr_value

from ase.atoms import Atoms
from ase.data import chemical_symbols
from ase.db.row import AtomsRow

__all__ = (
    'IncompatibleObjectError',
    'IncompatibleObjectTypeError',
    'IncompatibleObjectValueError',
    'NDArray',
    'ChemicalSymbol',
    'Species',
    'SpeciesConstrained',
    'Stoichiometry',
    'StoichiometryAcceptedArgs',
)


class IncompatibleObjectError(Exception):
    """
    Error type raised by `Stoichiometry` if it encounters an object that
    it does not know how to handle.
    """
    def __init__(self, obj: typing.Any, *args, **kwargs) -> None:
        """
        Parameters
        ----------
        obj
            The object triggering the error;
            only `obj` appears in the error message
        *args
            `(obj, *args)` are kept at the `.all_arguments` attribute.
        **kwargs
            (Ignored)
        """
        super().__init__(f'obj = {obj!r}: cannot cast to `Stoichiometry`')
        self._all_args = (obj, *args)

    @property
    def all_arguments(self) -> typing.Tuple:
        return self._all_args


class IncompatibleObjectTypeError(IncompatibleObjectError, TypeError):
    pass


class IncompatibleObjectValueError(IncompatibleObjectError, ValueError):
    pass


Stoichiometry = typing.TypeVar('Stoichiometry', bound='Stoichiometry')
ChemicalSymbol = typing.TypeVar(
    'ChemicalSymbol', bound=typing.Literal[tuple(chemical_symbols)],
)
Species = typing.Union[numbers.Integral, ChemicalSymbol]
SpeciesConstrained = typing.TypeVar(
    'SpeciesConstrained', numbers.Integral, ChemicalSymbol,
)
StoichiometryAcceptedArgs = typing.Union[
    typing.Mapping[Species, numbers.Integral],
    Stoichiometry,
    typing.Collection[
        typing.Union[Species, typing.Tuple[Species, numbers.Integral]]
    ],
    Atoms,
    AtomsRow,
    None,
    typing.Any,
]
