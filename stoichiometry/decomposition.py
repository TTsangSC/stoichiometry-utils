"""
Utitilities for decomposing stoichiometries.
"""
import numbers
import typing

import numpy as np

from ._types import (
    StoichiometryAcceptedArgs, SpeciesConstrained, ChemicalSymbol, NDArray,
)
from .core import Stoichiometry, list_all_elements, vectorize_atoms
from .gaussian_elimination import gaussian_elimination

__all__ = ('stoichiometrize_species',)


@typing.overload
def stoichiometrize_species(
    species: StoichiometryAcceptedArgs,
    basis_species: typing.Sequence[StoichiometryAcceptedArgs],
    use_symbols: typing.Literal[True] = True,
    exact: bool = False,
) -> typing.Tuple[np.ndarray, typing.Dict[ChemicalSymbol, numbers.Real]]:
    ...


@typing.overload
def stoichiometrize_species(
    species: StoichiometryAcceptedArgs,
    basis_species: typing.Sequence[StoichiometryAcceptedArgs],
    use_symbols: typing.Literal[False] = False,
    exact: bool = False,
) -> typing.Tuple[np.ndarray, typing.Dict[numbers.Integral, numbers.Real]]:
    ...


@typing.overload
def stoichiometrize_species(
    species: StoichiometryAcceptedArgs,
    basis_species: typing.Sequence[StoichiometryAcceptedArgs],
    use_symbols: bool = False,
    exact: typing.Literal[True] = True,
) -> typing.Tuple[
    NDArray[numbers.Rational],
    typing.Dict[SpeciesConstrained, numbers.Rational],
]:
    ...


@typing.overload
def stoichiometrize_species(
    species: StoichiometryAcceptedArgs,
    basis_species: typing.Sequence[StoichiometryAcceptedArgs],
    use_symbols: bool = False,
    exact: typing.Literal[False] = False,
) -> typing.Tuple[
    NDArray[numbers.Real], typing.Dict[SpeciesConstrained, numbers.Real],
]:
    ...


def stoichiometrize_species(
    species: StoichiometryAcceptedArgs,
    basis_species: typing.Sequence[StoichiometryAcceptedArgs],
    use_symbols: bool = False,
    exact: bool = False,
) -> typing.Tuple[
    typing.Union[NDArray[numbers.Real], NDArray[numbers.Rational]],
    typing.Dict[SpeciesConstrained, numbers.Real],
]:
    """
    Try to express the atomic makeup of `species` as a linear
    combination of the list of `basis_species`.

    Arguments
    ---------
    species
        Object representing a stoichiometry
    basis_species
        Objects representing stoichiometries
    use_symbols
        Whether to use chemical-symbol (if true) or atomic-number keys
        in the returned dictionary of residues
    exact
        Whether to use floating-point (if false) or arbitrary-precision
        fractional (if true) arithmatics to calculate and represent the
        coefficients

    Return
    ------
    Tuple

    >>> (  # noqa: F821 # doctest: +SKIP
    ...     array_of_coeffcients,
    ...     {atomic_symbol_str_or_atomic_number: residual}
    ... )

    Example
    -------
    Floating-point math:

    >>> from fractions import Fraction
    >>>
    >>> import numpy as np
    >>>
    >>> *basis_species, acetaldehyde = 'C3H8 H2 H2O CH3CHO'.split()
    >>> true_coeffs = [  # 3 CH3CHO + 5 H2 <-> 2 C3H8 + 3 H2O
    ...     Fraction(2, 3), Fraction(-5, 3), 1
    ... ]
    >>> coeffs_float, res_float = stoichiometrize_species(
    ...     acetaldehyde, basis_species,
    ... )
    >>> assert np.allclose(
    ...     coeffs_float, [float(c) for c in true_coeffs],
    ... ), coeffs_float
    >>> assert np.isclose(list(res_float.values()), 0).all(), res_float
    >>> assert any(  # Residues because FP math is not exact
    ...     res_float.values()
    ... )

    Fractional arithmatics:

    >>> coeffs_frac, res_frac = stoichiometrize_species(
    ...     acetaldehyde, basis_species, exact=True,
    ... )
    >>> assert (list(coeffs_frac) == true_coeffs), coeffs_frac
    >>> assert all(not r for r in res_frac.values()), res_frac

    Forms of the dictionary of residues:

    >>> res_float  # doctest: +ELLIPSIS
    {1: ..., 6: ..., 8: ...}
    >>> stoichiometrize_species(  # doctest: +ELLIPSIS
    ...     acetaldehyde, basis_species, use_symbols=True,
    ... )[1]
    {'H': ..., 'C': ..., 'O': ...}
    """
    species, *basis_species = [
        Stoichiometry(s) for s in (species, *basis_species)
    ]

    elements_list = list_all_elements(
        species, *basis_species, use_symbols=use_symbols,
    )

    vector = vectorize_atoms(species, elements_list)
    basis = np.array([
        vectorize_atoms(bs, elements_list) for bs in basis_species
    ])

    coefficients = project(vector, basis, fp=(not exact))
    residues = dict(zip(elements_list, vector - coefficients.dot(basis)))
    return coefficients, residues


@typing.overload
def project(
    v: typing.Union[typing.Sequence[numbers.Real], NDArray[numbers.Real]],
    b: typing.Union[
        typing.Sequence[typing.Sequence[numbers.Real]], NDArray[numbers.Real],
    ],
    fp: typing.Literal[True],
) -> NDArray[numbers.Real]:
    ...


@typing.overload
def project(
    v: typing.Union[
        typing.Sequence[numbers.Rational], NDArray[numbers.Rational],
    ],
    b: typing.Union[
        typing.Sequence[typing.Sequence[numbers.Rational]],
        NDArray[numbers.Rational],
    ],
    fp: typing.Literal[False],
) -> NDArray[numbers.Rational]:
    ...


def project(
    v: typing.Union[
        typing.Sequence[numbers.Real],
        NDArray[numbers.Real],
        typing.Sequence[numbers.Rational],
        NDArray[numbers.Rational],
    ],
    b: typing.Union[
        typing.Sequence[typing.Sequence[numbers.Real]],
        NDArray[numbers.Real],
        typing.Sequence[typing.Sequence[numbers.Rational]],
        NDArray[numbers.Rational],
    ],
    fp: bool = True,
) -> typing.Union[NDArray[numbers.Real], NDArray[numbers.Rational]]:
    """
    Get the coefficients of the projection of a vector onto the span of
    the provided basis.

    Parameters
    ----------
    v
        Real vector
    b
        Real basis vectors
    fp
        Whether to use floating-point math (faster but not exact)

    Return
    ------
    1-D array of real coefficients

    Example
    -------
    >>> import numpy as np
    >>>
    >>> rng = np.random.default_rng(42)
    >>> basis = rng.random((3, 5))
    >>> basis[:, -1] = 0  # Now [0, 0, 0, 0, 1] is the kernel of `basis`
    >>> coefficients = rng.random(3)
    >>> vector = (  # Added component is orthogonal to `basis`
    ...     coefficients.dot(basis) + [0, 0, 0, 0, rng.random()]
    ... )
    >>> assert np.allclose(project(vector, basis), coefficients)
    """
    a_b = np.array(b)
    if not (a_b.shape and a_b.shape[0] and len(a_b.shape) == 2):
        raise TypeError(f'b = {b!r}: expected a sequence/an array of vectors')
    if a_b.shape[1] != len(v):
        raise ValueError(
            f'b = {b!r}: basis vector lengths inconsistent with v = {v!r}'
        )
    if not all(
        isinstance(comp, numbers.Real) for vec in (v, *b) for comp in vec
    ):
        raise TypeError(f'v = {v!r}, b = {b!r}: vectors must be real')
    if fp:
        return np.linalg.solve(a_b.dot(a_b.T), a_b.dot(v))
    _, inv_bbt = gaussian_elimination(
        a_b.dot(a_b.T), float_fallback=False, mpa=True,
    )
    return inv_bbt.dot(a_b.dot(v))
