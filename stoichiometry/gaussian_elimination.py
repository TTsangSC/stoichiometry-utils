import inspect
import numbers
import types
import typing
import warnings
from fractions import Fraction
from math import gcd
from traceback import format_exception
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal

import numpy as np

__all__ = 'gaussian_elimination',

Matrix = typing.Union[
    np.ndarray, typing.Sequence[typing.Sequence[numbers.Complex]]
]

NUMPY_SETERR_BEHAVIORS = 'ignore', 'warn', 'raise', 'call', 'print', 'log'
NUMPY_SETERR_CATEGORIES = 'all', 'divide', 'over', 'under', 'invalid'
NumpySetErrBehavior = Literal[NUMPY_SETERR_BEHAVIORS]
NumpySetErrCategories = Literal[NUMPY_SETERR_CATEGORIES]


def lcm(
    int_1: typing.Union[
        np.ndarray, typing.Collection[numbers.Integral], numbers.Integral
    ],
    int_2: typing.Optional[numbers.Integral] = None,
    *ints: typing.Sequence[numbers.Integral],
) -> typing.Union[numbers.Integral, typing.Tuple[np.ndarray, numbers.Integral]]:
    """
    Return the leact common multiple of the supplied integers.

    Call signatures
    ---------------
    Numbers as positional args:

    >>> lcm(int, int, [int, ...])  # doctest: +SKIP

    Numbers as single positional arg:

    >>> lcm((int, int, [int, ...]))  # doctest: +SKIP

    Return
    ------
    Least common multiple of the given integers

    Example
    -------
    >>> lcm([2, 4, 6, 8, 10])
    120
    >>> lcm(1, 3, 5, 7, 9)
    315
    """
    error = TypeError(
        'int_1, int_2, *ints = ' +
        ', '.join(repr(i) for i in [int_1, int_2, *ints]) +
        ': ' +
        'expected call signatures of either `lcm(int, int, [int, ...])` or ' +
        '`lcm((int, int, [int, ...]))`'
    )
    if int_2 is None and not ints:
        integers = np.array(int_1).flatten()
    elif int_2 is not None:
        integers = np.array([int_1, int_2, *ints])
    else:
        raise error
    if not issubclass(integers.dtype.type, numbers.Integral):
        raise error
    if not (integers.ndim == 1 and len(integers) > 1):
        raise error
    cur_lcm = 1
    for i in integers:
        cur_lcm *= (i // gcd(cur_lcm, i))
    return cur_lcm


def exc_to_str(exception: Exception) -> str:
    return ''.join(
        format_exception(type(exception), exception, exception.__traceback__)
    )


numpy_seterr = typing.TypeVar('numpy_seterr', bound='numpy_seterr')


class numpy_seterr:
    """
    Wrapper around `numpy.seterr()`.

    Example
    -------
    >>> import numpy as np
    >>> import pytest
    >>>
    >>> np.geterr()  # doctest: +NORMALIZE_WHITESPACE
    {'divide': 'warn',
     'over': 'warn',
     'under': 'ignore',
     'invalid': 'warn'}
    >>> with numpy_seterr(  # doctest: +NORMALIZE_WHITESPACE
    ...     over='raise',
    ... ):
    ...     np.geterr()  # Value temporarily changed
    ...     with pytest.raises(OverflowError):
    ...         np.int64(123456789012345678901234567890)
    {'divide': 'warn',
     'over': 'raise',
     'under': 'ignore',
     'invalid': 'warn'}
    >>> # Now the old values should be restored
    >>> np.geterr()  # doctest: +NORMALIZE_WHITESPACE
    {'divide': 'warn',
     'over': 'warn',
     'under': 'ignore',
     'invalid': 'warn'}
    """
    arguments: typing.Mapping[NumpySetErrCategories, NumpySetErrBehavior]
    _arguments: typing.Tuple[
        typing.Tuple[NumpySetErrCategories, NumpySetErrBehavior], ...
    ]
    _replaced_arguments: typing.Union[
        typing.Tuple[
            typing.Tuple[NumpySetErrCategories, NumpySetErrBehavior], ...
        ],
        None
    ]

    def __init__(
        self,
        *args: typing.Sequence[NumpySetErrBehavior],
        **kwargs: typing.Mapping[
            NumpySetErrCategories, NumpySetErrBehavior
        ]
    ) -> None:
        arguments = {}
        for name, value in inspect.signature(self.__init__).bind(
            *args, **kwargs
        ).arguments.items():
            if value is None:
                continue
            if value not in NUMPY_SETERR_BEHAVIORS:
                raise TypeError(
                    f'{name} = {value!r}: ' +
                    f'expected any of the following: {NUMPY_SETERR_BEHAVIORS!r}'
                )
            arguments[name] = value
        self._arguments = tuple(arguments.items())
        self._replaced_arguments = None

    __init__.__signature__ = inspect.Signature(
        parameters=[
            inspect.Parameter(
                name='self', kind=inspect.Parameter.POSITIONAL_OR_KEYWORD,
            ),
            *(
                inspect.Parameter(
                    name=name,
                    default=None,
                    annotation=typing.Optional[NumpySetErrBehavior],
                    kind=inspect.Parameter.POSITIONAL_OR_KEYWORD,
                )
                for name in NUMPY_SETERR_CATEGORIES
            ),
        ],
        return_annotation=None,
    )

    def __enter__(self: numpy_seterr) -> numpy_seterr:
        self._replaced_arguments = tuple(np.seterr(**self.arguments).items())
        return self

    def __exit__(
        self,
        type: typing.Optional[typing.Type[Exception]] = None,
        value: typing.Optional[Exception] = None,
        traceback: typing.Optional[types.TracebackType] = None
    ) -> None:
        np.seterr(**dict(self._replaced_arguments))

    @property
    def arguments(self) -> typing.Mapping[
        NumpySetErrCategories, NumpySetErrBehavior
    ]:
        return types.MappingProxyType(dict(self._arguments))
    __slots__ = '_arguments', '_replaced_arguments'


def gaussian_elimination(
    matrix: Matrix,
    *,
    float_fallback: bool = True,
    mpa: bool = False,
) -> typing.Tuple[numbers.Real, Matrix]:
    """
    Perform Gaussian elimination on a square matrix to invert it.
    Integers (and optionally rational numbers) are preserved.

    Parameters
    ----------
    matrix
        2-D square matrix
    float_fallback
        Whether to fall back to floating-point numbers if `matrix` is
        rational but arithmatic errors with `fractions.Fraction` occurs;
        if false, an error is raised.
    mpa
        Whether to use arbitrary-precision arithmatics for rational
        `matrix`;
        if true, convert integers and fraction numerators/denominators
        to native python types instead of the fixed-size numpy types.

    Return
    ------
    Tuple of the determinent and the inverse matrix

    Notes
    -----
    - If `matrix` is all rational numbers the inversion and the
      determinent should be exact;
      else, we fall back to the `numpy.linalg` functions.
    - This is more intended for exactness than for speed;
      just use `numpy.linalg` etc. for performance-critical stuff.

    Example
    -------
    Integer arithmatics:

    >>> matrix = [
    ...     [2, 6, 7],
    ...     [-5, 3, 9],
    ...     [1, 8, 2],
    ... ]
    >>> cof = [
    ...     [-66, 19, -43],
    ...     [44, -3, -10],
    ...     [33, -53, 36],
    ... ]
    >>> det = 12 - 280 + 54 - 21 - 144 + 60
    >>> det_ge, inv_ge = gaussian_elimination(matrix)
    >>> assert (det == det_ge), (det, det_ge)
    >>> assert (
    ...     det_ge * inv_ge.T == cof
    ... ).all(), (det_ge * inv_ge.T, cof)

    Rational arithmatics:

    >>> from fractions import Fraction
    >>>
    >>> fcc = np.array([
    ...     Fraction(*pair) for pair in [
    ...         (0, 1), (1, 2), (1, 2),
    ...         (1, 2), (0, 1), (1, 2),
    ...         (1, 2), (1, 2), (0, 1),
    ...     ]
    ... ]).reshape((3, 3))
    >>> gaussian_elimination(fcc)  # doctest: +NORMALIZE_WHITESPACE
    (Fraction(1, 4),
     array([[-1,  1,  1],
            [ 1, -1,  1],
            [ 1,  1, -1]]))
    """
    # Function definitions
    def eliminate(i_diag: int, rng: range) -> numbers.Rational:
        """
        Perform row elimination on the rows in `rng` based on the
        diagonal entry at `[i_diag, i_diag]` and return the scaling
        factor on the determinent.

        Parameters
        ----------
        i_diag
            Diagonal entry to use
        rng
            Row indices where the `i_diag`-th row is to be used to
            eliminate the `i_diag`-th column;
            must be either all > `i_diag` or all < `i_diag`

        Return
        ------
        Factor by which the determinents of the matrices have been
        scaled by the row operations
        """
        scale = 1
        x_diag = matrix[i_diag, i_diag]
        if not x_diag:
            raise np.linalg.LinAlgError(f'matrix = {mat_repr}: singular matrix')
        assert i_diag < n
        assert all(i < i_diag for i in rng) or all(i > i_diag for i in rng)
        for i in rng:
            x = matrix[i, i_diag]
            if not x:
                continue
            # Note: elim_inner() takes the arguments, eliminates
            # matrix[i_diag, i] via row operations, and returns how much
            # the matrix is scaled; will be defined later
            scale *= elim_inner(x, x_diag, i, i_diag)
        return scale

    def make_native_fracs(x: numbers.Rational) -> Fraction:
        return Fraction(int(x.numerator), int(x.denominator))

    def _elim_inner_int(x: int, x_diag: int, i: int, i_diag: int) -> int:
        """Elimination with integer math (will scale rows)"""
        divisor = gcd(x_diag, x)
        x_diag //= divisor
        x //= divisor
        for mat in inverse, matrix:
            mat[i] = x_diag * mat[i] - x * mat[i_diag]
        return x_diag

    def _elim_inner_rational(
        x: numbers.Rational, x_diag: numbers.Rational, i: int, i_diag: int
    ) -> int:
        """Elimination with rational math (doesn't scale rows)"""
        for mat in inverse, matrix:
            mat[i] -= Fraction(x, x_diag) * mat[i_diag]
        return 1

    def _scale_diag_int(scaled_inv: Matrix, diagonal: np.ndarray) -> Matrix:
        """Explicitly construct new matrix because the input is int"""
        inv = np.array([
            [Fraction(x, x_diag) for x in row]
            for x_diag, row in zip(diagonal, scaled_inv)
        ])
        if all(x.denominator == 1 or x.numerator == 0 for x in np.ravel(inv)):
            return inv.astype(int)
        return inv

    def _scale_diag_rational(
        scaled_inv: Matrix, diagonal: np.ndarray
    ) -> Matrix:
        """Scale rows so that the product with matrix = iden. matrix"""
        return (scaled_inv.T/diagonal).T

    # Basic input verification
    matrix, matrix_orig = np.array(matrix), matrix
    mat_repr = repr(matrix)
    if not (matrix.ndim == 2 and matrix.shape[0] == matrix.shape[1]):
        raise TypeError(f'matrix = {mat_repr}: not a 2-D square matrix')
    n = matrix.shape[0]
    diag_indices = (range(n), ) * 2
    # Exit point: if not rational, fall back to floating-point
    # arithmatics
    if issubclass(matrix.dtype.type, numbers.Integral):
        use_integral = True
    elif all(isinstance(x, numbers.Rational) for x in np.ravel(matrix)):
        use_integral = False
    else:
        matrix = matrix.astype(complex if matrix.imag.any() else float)
        return np.linalg.det(matrix), np.linalg.inv(matrix)
    # Behavior switches: choose implementations of matrix functions and
    # dtype of returned matrix
    if use_integral:
        elim_inner, scale_diag = _elim_inner_int, _scale_diag_int
        make_native = int
        inverse = np.identity(n, dtype=int)
    else:
        elim_inner, scale_diag = _elim_inner_rational, _scale_diag_rational
        make_native = make_native_fracs
        inverse = np.full((n, n), Fraction(0))
        inverse[diag_indices] = Fraction(1)
    # Guard against numpy fixed-size types
    if mpa:
        matrix = np.reshape(
            [make_native(x) for x in np.ravel(matrix)], matrix.shape,
        ).astype(object)
        inverse = inverse.astype(object)
    caught_errors = (ArithmeticError, ) if float_fallback else ()
    # Make upper triangular
    with numpy_seterr(over='raise'):
        try:
            det_scale = 1
            for i in range(n - 1):
                i_pivot = i + abs(matrix[i:, i]).argmax()
                if i != i_pivot:
                    for mat in inverse, matrix:
                        mat[[i, i_pivot]] = mat[[i_pivot, i]]
                    det_scale = -det_scale
                det_scale *= eliminate(i, range(i + 1, n))
            # Make lower triangular
            determinent = Fraction(matrix[diag_indices].prod(), det_scale)
            for i in range(n - 1, 0, -1):
                eliminate(i, range(i))
            # Scale diagonal
            diagonal = matrix[diag_indices]
            matrix[diag_indices] = 0
            assert not matrix.any()
            inverse = scale_diag(inverse, diagonal)
        except caught_errors as e:  # Fall-back for arithmatic errors
            msg = (
                f'Cannot solve matrix equation over rationals ({e!r}); ' +
                'falling back to floating-point \n' +
                exc_to_str(e)
            )
            warnings.warn(msg, RuntimeWarning)
            matrix = np.array(matrix_orig).astype(float)
            return np.linalg.det(matrix), np.linalg.inv(matrix)
    # Convert back to integers where appropriate
    if determinent.denominator == 1:
        determinent = determinent.numerator
        if mpa:
            determinent = int(determinent)
    if all(x.denominator == 1 or x.numerator == 0 for x in np.ravel(inverse)):
        inverse = inverse.astype(int)
        if mpa:
            inverse = inverse.astype(object)
    return determinent, inverse


def random_fraction_matrix(
    size: typing.Union[
        typing.Sequence[typing.Sequence[numbers.Integral]], np.ndarray,
    ],
    low: numbers.Integral = -2,
    high: numbers.Integral = 5,
    max_denominator: numbers.Integral = 7,
    seed: typing.Any = None,
    native: bool = True
) -> Matrix:
    """
    Generate a matrix of fractions for testing purposes.

    Parameters
    ----------
    size
        Dimensions of the output matrix
    low, high
        Lower and upper bounds for the matrix entries
    max_denominator
        Maximum denominator for the matrix entries
    seed
        Optional seed for the RNG
    native
        Whether to use the native `int` type in the fraction numerators
        and denominators instead of numpy integer types

    Return
    ------
    Matrix of `fractions.Fraction` objects

    Notes
    -----
    Beware of overflows when using fixed-size numpy integers
    (`native = False`).
    """
    dtype = object if native else int
    rng = np.random.default_rng(seed)
    integers = rng.integers(
        low=low, high=high - 1, size=size, endpoint=False,
    ).astype(dtype)
    n = np.prod(integers.shape)
    denominators, numerators = (
        rng.integers(low, max_denominator, size=n, endpoint=True).astype(dtype)
        for low in (2, 0)
    )
    numerators %= denominators
    return integers + np.reshape(
        [Fraction(n, d) for n, d in zip(numerators, denominators)],
        integers.shape,
    )
