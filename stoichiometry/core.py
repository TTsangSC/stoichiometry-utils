import functools
import numbers
import operator
import typing
from inspect import signature

import numpy as np
from ase.atoms import Atoms
from ase.data import atomic_numbers, chemical_symbols
from ase.db.row import AtomsRow
from ase.formula import Formula

from ._types import (
    IncompatibleObjectError, IncompatibleObjectTypeError,
    IncompatibleObjectValueError,
    NDArray, ChemicalSymbol, Species, SpeciesConstrained,
    Stoichiometry,
    StoichiometryAcceptedArgs,
)

__all__ = (
    'Stoichiometry',
    'vectorize_atoms',
    'list_all_elements',
)

# Helpers
_DUNDER_TABLE = dict(
    {
        name: f'{{}} {op} {{}}'
        for name, op in dict(
            {'or': '|', 'and': '&'},
            eq='==',
            ne='!=',
            gt='>',
            lt='<',
            ge='>=',
            le='<=',
            xor='^',
            add='+',
            sub='-',
            mul='*',
            truediv='/',
            floordiv='//',
            matmul='@',
        ).items()
    },
    divmod='divmod({}, {})',
)
# Typing info
_BroadcasterReturnType = typing.List[typing.Tuple[numbers.Integral, typing.Any]]
_StoichiometryCountType = typing.FrozenSet[
    typing.Tuple[numbers.Integral, numbers.Integral]
]


def _broadcast_over_elements(
    function: typing.Callable[[numbers.Integral, numbers.Integral], typing.Any],
) -> typing.Callable[
    [Stoichiometry, StoichiometryAcceptedArgs], _BroadcasterReturnType
]:
    """
    Return
    ------
    `func(s, x)` which, assuming `s` to be a `Stoichiometry`
    object itself, casts `x` to the same, then return a list of tuples
    in the form of

    >>> (  # noqa: F821 # doctest: +SKIP
    ...     atomic_number, function(count_in_s, count_in_x)
    ... )
    """
    def func(
        s: Stoichiometry,
        x: StoichiometryAcceptedArgs,
    ) -> _BroadcasterReturnType:
        x = type(s)(x)
        elements = list_all_elements(s, x)
        s_counts = vectorize_atoms(s, elements).tolist()
        x_counts = vectorize_atoms(x, elements).tolist()
        return [
            (number, function(s_count, x_count))
            for number, s_count, x_count in zip(elements, s_counts, x_counts)
        ]

    return func


def _stoichiometry_comparator(
    dunder: str,
) -> typing.Callable[[Stoichiometry, StoichiometryAcceptedArgs], bool]:
    """
    Return
    ------
    Dunder method `<dunder>(self, x)`, which casts `x` to a
    `Stoichiometry` object, then returns `True` only when the count for
    every element in the two species satisfies
    `<count_in_self>.<dunder>(<count_in_x>)`
    """
    def binary_op(self: Stoichiometry, x: StoichiometryAcceptedArgs) -> bool:
        return all(
            value for _, value in _broadcast_over_elements(op)(self, x)
        )

    op = getattr(operator, dunder)
    binary_op.__name__ = dunder
    binary_op.__qualname__ = 'Stoichiometry.' + dunder
    return binary_op


def _compare(
    comparator: typing.Callable[[typing.Any, typing.Any], bool],
    doc: typing.Optional[str] = None,
) -> typing.Callable[
    [typing.Any, typing.Any], typing.Union[bool, type(NotImplemented)]
]:
    """
    Convenience decorator so that the function returns `NotImplemented`
    if the comparison failed due to the raising of an
    `IncompatibleObjectError` (i.e. other operand cannot be converted to
    a stoichiometry)
    """
    @functools.wraps(comparator)
    def comparator_wrapper(*args, **kwargs) -> bool:
        try:
            return comparator(*args, **kwargs)
        except IncompatibleObjectError:
            return NotImplemented

    if doc is not None:
        comparator_wrapper.__doc__ = doc
    return comparator_wrapper


class Stoichiometry:
    """
    Object for representing the stoichiometry of an `ase.Atoms` object,
    supporting rich comparison by up-casting arguments.
    """
    def __init__(self, obj: StoichiometryAcceptedArgs) -> None:
        """
        Initialize from any of the following:

        - `Stoichiometry`
        - `ase.atoms.Atoms`
        - `ase.db.rows.AtomsRow`
        - `str` (chemical formula)
        - `None`
        - `Mapping[Species, int]` (atomic species -> nonneg. count)
        - `Iterable[Species]` (atomic species)
        - (Any other object the `ase.atoms.Atoms()` constructor takes)

        Where an atomic species can be given by either a string
        (chemical symbol) or an integer (atomic number).
        """
        converters = {
            # Specific types
            Stoichiometry: self._stoic2counts,
            Atoms: self._atoms2counts,
            AtomsRow: self._atomsrow2counts,
            str: self._str2counts,
            type(None): self._none2counts,
            # General types
            typing.Mapping: self._map2counts,
            typing.Collection: self._iter2counts,
        }
        # Find converter with highest precedence
        converter, error_cls = next(
            (
                (converter, IncompatibleObjectValueError)
                for cls, converter in converters.items()
                if isinstance(obj, cls)
            ),
            # Fallback
            (self._misc2counts, IncompatibleObjectTypeError),
        )
        try:
            self._counts = converter(obj)
        except Exception as e:
            raise error_cls(obj) from e

    # *************************** Iterator *************************** #

    def __iter__(self) -> typing.Generator[
        typing.Tuple[numbers.Integral, numbers.Integral], None, None
    ]:
        """
        Iterate over the elemental counts.

        Yield
        -----
        Tuple `(atomic_number, count)`

        See also
        --------
        `.iter()`
        """
        yield from self.iter(by_species=True, use_symbols=False)

    @typing.overload
    def iter(
        self,
        *,
        by_species: typing.Literal[True] = True,
        use_symbols: typing.Literal[True] = True,
    ) -> typing.Generator[
        typing.Tuple[ChemicalSymbol, numbers.Integral], None, None
    ]:
        ...

    @typing.overload
    def iter(
        self,
        *,
        by_species: typing.Literal[True] = True,
        use_symbols: typing.Literal[False] = False,
    ) -> typing.Generator[
        typing.Tuple[numbers.Integral, numbers.Integral], None, None
    ]:
        ...

    @typing.overload
    def iter(
        self,
        *,
        by_species: typing.Literal[False] = False,
        use_symbols: typing.Literal[True] = True,
    ) -> typing.Generator[ChemicalSymbol, None, None]:
        ...

    @typing.overload
    def iter(
        self,
        *,
        by_species: typing.Literal[False] = False,
        use_symbols: typing.Literal[False] = False,
    ) -> typing.Generator[numbers.Integral, None, None]:
        ...

    def iter(
        self,
        *,
        by_species: bool = True,
        use_symbols: bool = False,
    ) -> typing.Union[
        typing.Generator[SpeciesConstrained, None, None],
        typing.Generator[
            typing.Tuple[SpeciesConstrained, numbers.Integral], None, None
        ],
    ]:
        """
        Iterate over items.

        Parameters
        ----------
        by_species
            Whether to iterate over species and their counts (if true),
            or individual atoms of each species (if false)
        use_symbols
            Whether to use string chemical symbols (if true) or atomic
            numnbers (if false) to represent species

        Yield
        -----
        by_species = True
            `(species, count)` for each species
        by_species = False
            `species` for each atom
        where `species` are represented by either strings or numbers as
        described above

        Examples
        --------
        >>> acetic_acid = Stoichiometry('CH3COOH')
        >>> sorted(acetic_acid.iter())
        [(1, 4), (6, 2), (8, 2)]
        >>> sorted(acetic_acid.iter(by_species=False))
        [1, 1, 1, 1, 6, 6, 8, 8]
        >>> sorted(acetic_acid.iter(use_symbols=True))
        [('C', 2), ('H', 4), ('O', 2)]
        >>> ''.join(sorted(
        ...     acetic_acid.iter(by_species=False, use_symbols=True)
        ... ))
        'CCHHHHOO'
        """
        if use_symbols:
            def key(
                nc: typing.Tuple[numbers.Integral, numbers.Integral],
            ) -> ChemicalSymbol:
                return chemical_symbols[nc[0]]

            getter: typing.Callable[
                [numbers.Integral], ChemicalSymbol
            ] = functools.partial(operator.getitem, chemical_symbols)
        else:
            key, getter = None, int
        items = (
            (getter(number), count)
            for number, count in sorted(self._counts, key=key)
        )
        if by_species:
            return items
        return (s for species, count in items for s in (species,) * count)

    # ********************** Basic conversions *********************** #

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.formula!r})'

    def __str__(self) -> str:
        return self.formula

    def __len__(self) -> int:
        """
        Return
        ------
        Integer number of atoms

        Example
        -------
        >>> len(Stoichiometry('CH3COOH'))
        8
        """
        return sum(self.all_counts().values())

    def __bool__(self) -> bool:
        """
        Return
        ------
        Whether there are any atoms

        Example
        -------
        >>> [bool(Stoichiometry(x)) for x in (None, 'O2', [(1, 2)])]
        [False, True, True]
        """
        return bool([*self])

    def __hash__(self) -> int:
        return hash((type(self), *self))

    # ********************** Basic comparisons *********************** #

    @typing.overload
    def __eq__(self, x: StoichiometryAcceptedArgs) -> bool:
        ...

    @typing.overload
    def __eq__(self, x: typing.Any) -> typing.Literal[False]:
        ...

    for dunder, doc in dict(
        eq="""
        Equility test by interpreting the object as a stoichiometry.

        Parameters
        ----------
        x
            Object representing a stoichiometry

        Return
        ------
        Whether the object represents equal stoichiometries with the
        instance

        Notes
        -----
        If the comparison is invalid (i.e. the operand cannot be cast to
        a `Stoichiometry` object), return `NotImplemented`.

        Example
        -------
        >>> methane = Stoichiometry('CH4')
        >>> l = ['HHCHH', [1, 1, 6, ('H', 2)], dict(C=1, H=4), 'CO2']
        >>> [methane == x for x in l]
        [True, True, True, False]
        """,
        ge="""
        Non-strict containment test by interpreting the object as a
        stoichiometry;
        equivalent to `(self == x) or (self > x)`.

        Parameters
        ----------
        x
            Object representing a stoichiometry

        Return
        ------
        Whether the instance contains all atoms in the stoichiometry
        represented by the object
        """,
        le="""
        Non-strict subset test by interpreting the object as a
        stoichiometry;
        equivalent to `(self == x) or (self < x)`.

        Parameters
        ----------
        x
            Object representing a stoichiometry

        Return
        ------
        Whether the stoichiometry represented by the object contains all
        atoms in the instance
        """
    ).items():
        dunder = f'__{dunder}__'
        method = locals()[dunder] = _compare(
            _stoichiometry_comparator(dunder), doc=doc,
        )
        method.__signature__ = signature(method).replace(return_annotation=bool)
    del dunder, method, doc

    @_compare
    def __gt__(self, x: StoichiometryAcceptedArgs) -> bool:
        """
        Strict superset test by interpreting the object as a
        stoichiometry.

        Parameters
        ----------
        x
            Object representing a stoichiometry

        Return
        ------
        Whether the instance contains all atoms in the stoichiometry
        represented by the object and then more

        Example
        -------
        >>> water = Stoichiometry('H2O')
        >>> [water > x for x in ('H2O', 'H2', 'CO')]
        [False, True, False]
        """
        return (self >= x and self != x)

    @_compare
    def __lt__(self, x: StoichiometryAcceptedArgs) -> bool:
        """
        Strict subset test by interpreting the object as a
        stoichiometry.

        Parameters
        ----------
        x
            Object representing a stoichiometry

        Return
        ------
        Whether the stoichiometry represented by the object contains all
        atoms in the instance and then more

        Example
        -------
        >>> water = Stoichiometry('H2O')
        >>> [water < x for x in ('H2O', 'CH3CH3', 'H2CO')]
        [False, False, True]
        """
        return (self <= x and self != x)

    @_compare
    def __contains__(self, x: StoichiometryAcceptedArgs) -> bool:
        """
        Strict subset test by interpreting the object as a
        stoichiometry;
        equivalent to `self >= x`.

        Parameters
        ----------
        x
            Object representing a stoichiometry

        Example
        -------
        >>> water = Stoichiometry('H2O')
        >>> [x in water for x in ('H2O', 'CH3CH3', 'H2')]
        [True, False, True]
        """
        return self >= x

    # ********************** Binary operations *********************** #

    @_compare
    def __or__(self, x: StoichiometryAcceptedArgs) -> Stoichiometry:
        """
        Create stoichiometric union by interpreting the object as a
        stoichiometry.

        Parameters
        ----------
        x
            Object representing a stoichiometry

        Return
        ------
        Smallest stoichiometry `union` satisfying

        >>> self in union and x in union  # noqa: F821 # doctest: +SKIP

        Example
        -------
        >>> hydrogen = Stoichiometry('H2')
        >>> [(hydrogen | x).formula for x in ('O2', 'HCl', 'H')]
        ['H2O2', 'ClH2', 'H2']
        """
        return type(self)(_broadcast_over_elements(max)(self, x))

    @_compare
    def __ror__(self, x: StoichiometryAcceptedArgs) -> Stoichiometry:
        """
        Equivalent to `self | x`.
        """
        return self | x

    @_compare
    def __and__(self, x: StoichiometryAcceptedArgs) -> Stoichiometry:
        """
        Create stoichiometric intersection by interpreting the object as
        a stoichiometry.

        Parameters
        ----------
        x
            Object representing a stoichiometry

        Return
        ------
        Biggest stoichiometry `intersection` satisfying

        >>> (  # noqa: F821 # doctest: +SKIP
        ...     intersection in self and intersection in x
        ... )

        Example
        -------
        >>> hydrogen = Stoichiometry('H2')
        >>> [(hydrogen & x).formula for x in ('O2', 'HCl', 'H2O2')]
        ['', 'H', 'H2']
        """
        return type(self)(_broadcast_over_elements(min)(self, x))

    @_compare
    def __rand__(self, x: StoichiometryAcceptedArgs) -> Stoichiometry:
        """
        Equivalent to `self & x`.
        """
        return self & x

    @_compare
    def __xor__(self, x: StoichiometryAcceptedArgs) -> Stoichiometry:
        """
        Create stoichiometric symmetric difference by interpreting the
        object as a stoichiometry.

        Parameters
        ----------
        x
            Object representing a stoichiometry

        Return
        ------
        Symmetric-difference stoichiometry `symdiff` such that

        >>> (  # noqa: F821 # doctest: +SKIP
        ...     symdiff + (self & x) == (self | x)
        ... )

        Example
        -------
        >>> aldehyde = Stoichiometry('H2CO')
        >>> [(aldehyde ^ x).formula for x in ('H2O', 'HCl', 'CH4')]
        ['C', 'CClHO', 'H2O']
        """
        def abs_diff(
            count_1: numbers.Integral, count_2: numbers.Integral
        ) -> numbers.Integral:
            return abs(count_1 - count_2)

        return type(self)(_broadcast_over_elements(abs_diff)(self, x))

    @_compare
    def __rxor__(self, x: StoichiometryAcceptedArgs) -> Stoichiometry:
        """
        Equivalent to `self ^ x`.
        """
        return self ^ x

    @_compare
    def __add__(self, x: StoichiometryAcceptedArgs) -> Stoichiometry:
        """
        Create stoichiometric sum by interpreting the object as a
        stoichiometry.

        Parameters
        ----------
        x
            Object representing a stoichiometry

        Return
        ------
        Stoichiometry with the count of each species equal to the sum of
        its counts in the operands

        Example
        -------
        >>> (Stoichiometry('H2O') + 'C2H4') == 'CH3CH2OH'
        True
        """
        return type(self)(_broadcast_over_elements(operator.add)(self, x))

    @_compare
    def __radd__(self, x: StoichiometryAcceptedArgs) -> Stoichiometry:
        """
        Equivalent to `self + x`.
        """
        return self + x

    @_compare
    def __sub__(self, x: StoichiometryAcceptedArgs) -> Stoichiometry:
        """
        Create stoichiometric difference by interpreting the object as a
        stoichiometry.

        Parameters
        ----------
        x
            Object representing a stoichiometry

        Return
        ------
        Stoichiometry with the count of each species equal to the capped
        difference (>= 0) of its counts in the operands

        Example
        -------
        >>> ethanol, peroxide, ethylene = (
        ...     Stoichiometry(formula)
        ...     for formula in 'CH3CH2OH H2O2 C2H4'.split()
        ... )
        >>> (  # Subtrahend has more O than minuend -> ceil(diff, 0)
        ...     ethanol - peroxide
        ... ) == ethylene
        True
        """
        def capped_sub(
            count_1: numbers.Integral, count_2: numbers.Integral
        ) -> numbers.Integral:
            return max(0, count_1 - count_2)

        return type(self)(_broadcast_over_elements(capped_sub)(self, x))

    @_compare
    def __rsub__(self, x: StoichiometryAcceptedArgs) -> Stoichiometry:
        """
        Equivalent to `Stoichiometry(x) - self`.
        """
        return type(self)(x) - self

    @_compare
    def __mul__(self, x: numbers.Integral) -> Stoichiometry:
        """
        Create stoichiometric product by multiplying the instance with a
        constant.

        Parameters
        ----------
        x
            Non-negative integer

        Return
        ------
        Stoichiometry with the count of each species equal to its count
        in the instance multiplied by the constant
        """
        if isinstance(x, numbers.Integral) and x >= 0:
            return type(self)([(number, count * x) for number, count in self])
        else:
            raise TypeError(f'x = {x!r}: must be a non-negative integer')

    @_compare
    def __rmul__(self, x: StoichiometryAcceptedArgs) -> Stoichiometry:
        """
        Equivalent to `self * x`.
        """
        return self * x

    @typing.overload
    def __truediv__(self, x: numbers.Integral) -> Stoichiometry:
        ...

    @typing.overload
    def __truediv__(self, x: StoichiometryAcceptedArgs) -> numbers.Integral:
        ...

    @_compare
    def __truediv__(
        self, x: typing.Union[numbers.Integral, StoichiometryAcceptedArgs],
    ) -> typing.Union[Stoichiometry, numbers.Integral]:
        """
        Create stoichiometric quotient by dividing the instance by a
        constant or an object representing a stoichiometry.

        Parameters
        ----------
        x
            Positive integer or object representing a stoichiometry

        Return
        ------
        If divisible by `x`
            Quotient stoichiometry or integer
        Else
            (Error raised)

        Example
        -------
        >>> glucose = Stoichiometry('C6H12O6')
        >>> glucose/'H2CO'
        6
        >>> (glucose/2).formula
        'C3H6O3'
        >>> glucose/4
        Traceback (most recent call last):
          ...
        ValueError: Stoichiometry('C6H12O6'): not divisible by `x = 4`

        See also
        --------
        `.__divmod__()`
        """
        quotient, mod = divmod(self, x)
        if mod:
            raise ValueError(f'{self!r}: not divisible by `x = {x!r}`')
        return quotient

    @_compare
    def __rtruediv__(self, x: StoichiometryAcceptedArgs) -> numbers.Integral:
        """
        Equivalent to `Stoichiometry(x) / self`.
        """
        return type(self)(x) / self

    @typing.overload
    def __divmod__(
        self, x: numbers.Integral,
    ) -> typing.Tuple[Stoichiometry, Stoichiometry]:
        ...

    @typing.overload
    def __divmod__(
        self, x: StoichiometryAcceptedArgs,
    ) -> typing.Tuple[numbers.Integral, Stoichiometry]:
        ...

    @_compare
    def __divmod__(
        self, x: typing.Union[numbers.Integral, StoichiometryAcceptedArgs],
    ) -> typing.Tuple[
        typing.Union[Stoichiometry, numbers.Integral], Stoichiometry,
    ]:
        """
        Do long division with an integer or a stoichiometry.

        Parameters
        ----------
        x
            Non-negative integer or object representing a stoichiometry

        Return
        ------
        Tuple `(quotient, stoichiometric_mod)`, where
        if `x` is a positive integer count:

        >>> (  # noqa: F821 # doctest: +SKIP
        ...     self == quotient * x + stoichiometric_mod
        ... )

        if `x` represents a stoichiometry:

        >>> self == (  # noqa: F821 # doctest: +SKIP
        ...     quotient * Stoichiometry(x) + stoichiometric_mod
        ... )

        where in each case the `quotient` is maximized;
        in any other case, either an `IncompatibleObjectError` or
        `ValueError` is raised

        Example
        -------
        >>> glycerol = Stoichiometry('HOCH2CHOHCH2OH')  # C3H8O3
        >>> divmod(glycerol, 2)
        (Stoichiometry('CH4O'), Stoichiometry('CO'))
        >>> divmod(glycerol, 'CH4')
        (2, Stoichiometry('CO3'))

        Notes
        -----
        The result is equivalent to that of

        >>> self // x, self % x;  # noqa: F821 # doctest: +SKIP

        in fact, both the `.__floordiv__()` and `.__mod__()` methods
        depend on this method.
        """
        err = ValueError(
            f'x = {x!r}: '
            'expected a positive integer or an object castable to a '
            'non-empty stoichiometry'
        )
        if isinstance(x, numbers.Integral):
            if not x > 0:
                raise err
            div, mod = zip(*(
                zip((element, ) * 2, divmod(count, x))
                for element, count in self.all_counts().items()
            ))
            return type(self)(div), type(self)(mod)
        stoic_x = Stoichiometry(x)
        if not stoic_x:
            raise err
        s_counts, x_counts = self.all_counts(), stoic_x.all_counts()
        if not set(x_counts) <= set(s_counts):
            # Not all species in x found
            return 0, self
        quotient = min(
            s_counts[element] // x_count
            for element, x_count in x_counts.items()
        )
        m_counts = s_counts.copy()
        for element, x_count in x_counts.items():
            m_counts[element] -= quotient * x_count
        return quotient, type(self)(m_counts)

    def __rdivmod__(
        self, x: StoichiometryAcceptedArgs,
    ) -> typing.Tuple[numbers.Integral, Stoichiometry]:
        """
        Equivalent to `divmod(Stoichiometry(x), self)`.
        """
        return divmod(type(self)(x), self)

    @typing.overload
    def __floordiv__(self, x: numbers.Integral) -> Stoichiometry:
        ...

    @typing.overload
    def __floordiv__(self, x: StoichiometryAcceptedArgs) -> numbers.Integral:
        ...

    @_compare
    def __floordiv__(
        self, x: typing.Union[numbers.Integral, StoichiometryAcceptedArgs],
    ) -> typing.Union[Stoichiometry, numbers.Integral]:
        """
        Create floored stoichiometric quotient by dividing the instance
        by a constant or an object representing a stoichiometry.

        Parameters
        ----------
        x
            Positive integer or object representing a stoichiometry

        Return
        ------
        Quotient stoichiometry or integer

        See also
        --------
        `.__divmod__()`
        """
        return divmod(self, x)[0]

    @_compare
    def __rfloordiv__(self, x: StoichiometryAcceptedArgs) -> numbers.Integral:
        """
        Equivalent to `Stoichiometry(x) // self`.
        """
        return type(self)(x) // self

    @_compare
    def __mod__(
        self, x: typing.Union[numbers.Integral, StoichiometryAcceptedArgs],
    ) -> Stoichiometry:
        """
        Get the stoichiometric remainder when dividing the instance by a
        constant or an object representing a stoichiometry.

        Parameters
        ----------
        x
            Positive integer or object representing a stoichiometry

        Return
        ------
        Remainder stoichiometry

        See also
        --------
        `.__divmod__()`
        """
        return divmod(self, x)[1]

    @_compare
    def __rmod__(self, x: StoichiometryAcceptedArgs) -> Stoichiometry:
        """
        Equivalent to `Stoichiometry(x) % self`.
        """
        return type(self)(x) % self

    # Write default docstrings
    for op, op_pattern in _DUNDER_TABLE.items():
        for r, lhs, rhs in ('', 'self', 'x'), ('r', 'x', 'self'):
            dunder = f'__{r}{op}__'
            if (
                dunder in locals() and
                not getattr(locals()[dunder], '__doc__', None)
            ):
                locals()[dunder].__doc__ = 'Return `{}`. '.format(
                    op_pattern.format(lhs, rhs)
                )
    del dunder, r, lhs, rhs, op, op_pattern

    # ************************ Misc. methods ************************* #

    def count(self, element: Species) -> numbers.Integral:
        """
        Get the number of times an element occurs.

        Parameters
        ----------
        element
            Integer atomic number or string chemical symmbol

        Return
        ------
        Integer count

        Example
        -------
        >>> benzene = Stoichiometry('C6H6')
        >>> benzene.count('C')
        6
        >>> benzene.count(5)  # N
        0
        """
        if isinstance(element, numbers.Integral):
            use_symbols = False
        elif isinstance(element, str):
            use_symbols = True
        else:
            raise TypeError(
                f'element = {element!r}: '
                'expected a string or a positive integer'
            )
        return self.all_counts(use_symbols=use_symbols).get(element, 0)

    @typing.overload
    def all_counts(
        self, use_symbols: typing.Literal[True] = True,
    ) -> typing.Dict[ChemicalSymbol, numbers.Integral]:
        ...

    @typing.overload
    def all_counts(
        self, use_symbols: typing.Literal[False] = False,
    ) -> typing.Dict[numbers.Integral, numbers.Integral]:
        ...

    def all_counts(
        self, use_symbols: bool = False,
    ) -> typing.Dict[SpeciesConstrained, numbers.Integral]:
        """
        Get all the species counts.

        Parameters
        ----------
        use_symbols
            Whether to represent species with string chemical symbols
            (if true) of integer atomic numbers (if false)

        Return
        ------
        Dictionary `{atomic_symbol_or_number: count}`

        See also
        --------
        `.iter()`
        """
        return dict(self.iter(by_species=True, use_symbols=use_symbols))

    @typing.overload
    def all_atoms(
        self, use_symbols: typing.Literal[True] = True,
    ) -> typing.List[ChemicalSymbol]:
        ...

    @typing.overload
    def all_atoms(
        self, use_symbols: typing.Literal[False] = False,
    ) -> typing.List[numbers.Integral]:
        ...

    def all_atoms(
        self, use_symbols: bool = False,
    ) -> typing.List[SpeciesConstrained]:
        """
        Get all the atoms.

        Parameters
        ----------
        use_symbols
            Whether to represent species with string chemical symbols
            (if true) of integer atomic numbers (if false)

        Return
        ------
        List `[atomic_symbol_or_number]`

        See also
        --------
        `.iter()`

        Example
        -------
        >>> benzene = Stoichiometry('C6H6')
        >>> benzene.all_atoms()
        [1, 1, 1, 1, 1, 1, 6, 6, 6, 6, 6, 6]
        >>> benzene.all_atoms(use_symbols=True)
        ['C', 'C', 'C', 'C', 'C', 'C', 'H', 'H', 'H', 'H', 'H', 'H']
        """
        return list(self.iter(by_species=False, use_symbols=use_symbols))

    # *********************** Internal methods *********************** #

    @classmethod
    def _map2counts(
        cls, m: typing.Mapping[Species, numbers.Integral],
    ) -> _StoichiometryCountType:
        return cls._iter2counts(m.items())

    @classmethod
    def _stoic2counts(cls, s: Stoichiometry) -> _StoichiometryCountType:
        return cls._map2counts(s.all_counts())

    @classmethod
    def _atoms2counts(cls, a: Atoms) -> _StoichiometryCountType:
        return cls._map2counts(a.symbols.formula.count())

    @classmethod
    def _atomsrow2counts(cls, r: AtomsRow) -> _StoichiometryCountType:
        return cls._map2counts(r.count_atoms())

    @classmethod
    def _none2counts(cls, n: None) -> _StoichiometryCountType:
        return cls._iter2counts(())

    @classmethod
    def _str2counts(cls, s: str) -> _StoichiometryCountType:
        """
        Notes
        -----
        It is illegal for a non-empty string to be resolved to an empty
        stoichiometry dict (i.e. it's okay for element counts to be
        zero, but there must be at least one element).
        """
        counts = Formula(s).count()
        if s and not counts:
            raise ValueError(f's = {s!r}: non-empty but no elements specified')
        return cls._map2counts(counts)

    @classmethod
    def _iter2counts(
        cls,
        i: typing.Iterable[
            typing.Union[Species, typing.Tuple[Species, numbers.Integral]]
        ]
    ) -> _StoichiometryCountType:
        d = {}
        for item in i:
            if isinstance(item, (numbers.Integral, str)):
                species, count = item, 1
            else:
                species, count = item
            number = atomic_numbers.get(species, species)
            if (
                isinstance(number, numbers.Integral) and
                0 <= number <= max(atomic_numbers.values()) and
                isinstance(count, numbers.Integral) and
                0 <= count
            ):
                d[number] = d.get(number, 0) + count
                continue
            raise TypeError(
                f'atoms = {i!r}: '
                'expected items to be non-negative integers '
                f'<= {max(atomic_numbers.values())}, '
                'atomic symbols, or '
                '`(atomic_symbol_or_number, nonnegative_int_count)`'
            )
        return frozenset((k, v) for k, v in d.items() if v)

    @classmethod
    def _misc2counts(cls, m: typing.Any) -> _StoichiometryCountType:
        return cls._atoms2counts(Atoms(m))

    # ************************* Descriptors ************************** #

    @property
    def formula(self) -> str:
        """
        The chemical formula.
        """
        return ''.join(
            symbol if count == 1 else f'{symbol}{count}'
            for symbol, count in self.iter(
                by_species=True, use_symbols=True,
            )
        )


# Other functions


def vectorize_atoms(
    atoms: StoichiometryAcceptedArgs, elements: typing.Sequence[Species],
) -> NDArray[numbers.Integral]:
    """
    Turn `atoms` into a vector of the numbers of atoms of each element
    on the list of `elements`.

    Parameters
    ----------
    atoms
        Object representing a stoichiometry
    elements
        Sequence of string chemical symbols or atomic numbers

    Return
    ------
    1-D array of counts

    Example
    -------
    >>> for species in 'C2H4', 'NH3', 'H2NCONH2':
    ...     print(species, list(vectorize_atoms(species, 'CHON')))
    ...
    C2H4 [2, 4, 0, 0]
    NH3 [0, 3, 0, 1]
    H2NCONH2 [1, 4, 1, 2]
    """
    if not all(isinstance(e, (numbers.Integral, str)) for e in elements):
        raise TypeError(
            f'elements = {elements!r}: '
            'entries in `elements` must all be atomic numbers or symbols'
        )
    elements = [
        e if isinstance(e, numbers.Integral) else atomic_numbers[e]
        for e in elements
    ]
    d = Stoichiometry(atoms).all_counts(use_symbols=False)
    return np.array([d.get(element, 0) for element in elements])


@typing.overload
def list_all_elements(
    *species: typing.Sequence[StoichiometryAcceptedArgs],
    use_symbols: typing.Literal[True] = True,
) -> typing.List[ChemicalSymbol]:
    ...


@typing.overload
def list_all_elements(
    *species: typing.Sequence[StoichiometryAcceptedArgs],
    use_symbols: typing.Literal[False] = False,
) -> typing.List[numbers.Integral]:
    ...


def list_all_elements(
    *species: typing.Sequence[StoichiometryAcceptedArgs],
    use_symbols: bool = False,
) -> typing.List[SpeciesConstrained]:
    """
    Get elements present in the provided stoichiometries.

    Parameters
    ----------
    *species
        Objects representing stoichiometries
    use_symbols
        Whether to use string chemical symbols (if true) or atomic
        numbers (if false) to represent species

    Return
    ------
    List of species

    Example
    -------
    >>> list_all_elements('CH4', 'NH2', 'Co2O3')
    [1, 6, 7, 8, 27]
    >>> list_all_elements('CH4', 'NH2', 'Co2O3', use_symbols=True)
    ['H', 'C', 'N', 'O', 'Co']
    """
    results = set()
    for i, s in enumerate(species):
        s = Stoichiometry(s)
        results |= set(s.all_counts(use_symbols=False))
    numbers = sorted(results)
    return [chemical_symbols[n] for n in numbers] if use_symbols else numbers
